package grpc

import (
	"market_system/ms_go_user_service/config"
	"market_system/ms_go_user_service/genproto/user_service"
	"market_system/ms_go_user_service/grpc/client"
	"market_system/ms_go_user_service/grpc/service"
	"market_system/ms_go_user_service/pkg/logger"
	"market_system/ms_go_user_service/storage"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {

	grpcServer = grpc.NewServer()

	user_service.RegisterUserServiceServer(grpcServer, service.NewUserService(cfg, log, strg, srvc))
	user_service.RegisterBranchServiceServer(grpcServer, service.NewBranchService(cfg, log, strg, srvc))
	user_service.RegisterPointOfSaleServiceServer(grpcServer, service.NewPointOfSaleService(cfg, log, strg, srvc))
	user_service.RegisterProviderServiceServer(grpcServer, service.NewProviderService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)
	return
}
