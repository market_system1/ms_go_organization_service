package service

import (
	"context"
	"fmt"
	"market_system/ms_go_user_service/config"
	"market_system/ms_go_user_service/genproto/user_service"
	"market_system/ms_go_user_service/grpc/client"
	"market_system/ms_go_user_service/pkg/logger"
	"market_system/ms_go_user_service/storage"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type PointOfSaleService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*user_service.UnimplementedPointOfSaleServiceServer
}

func NewPointOfSaleService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *PointOfSaleService {
	return &PointOfSaleService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *PointOfSaleService) Create(ctx context.Context, req *user_service.CreatePointOfSale) (resp *user_service.PointOfSale, err error) {

	i.log.Info("---CreatePointOfSale------>", logger.Any("req", req))

	pKey, err := i.strg.PointOfSale().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreatePointOfSale->PointOfSale->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.PointOfSale().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyPointOfSale->PointOfSale->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *PointOfSaleService) GetByID(ctx context.Context, req *user_service.PointOfSalePrimaryKey) (resp *user_service.PointOfSale, err error) {

	i.log.Info("---GetPointOfSaleByID------>", logger.Any("req", req))

	resp, err = i.strg.PointOfSale().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetPointOfSaleByID->PointOfSale->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *PointOfSaleService) GetList(ctx context.Context, req *user_service.GetListPointOfSaleRequest) (resp *user_service.GetListPointOfSaleResponse, err error) {

	i.log.Info("---GetPointOfSales------>", logger.Any("req", req))

	resp, err = i.strg.PointOfSale().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetPointOfSales->PointOfSale->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *PointOfSaleService) Update(ctx context.Context, req *user_service.UpdatePointOfSale) (resp *user_service.PointOfSale, err error) {

	i.log.Info("---UpdatePointOfSale------>", logger.Any("req", req))
	fmt.Printf("%+v\n", req)

	rowsAffected, err := i.strg.PointOfSale().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdatePointOfSale--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.PointOfSale().GetByPKey(ctx, &user_service.PointOfSalePrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetPointOfSale->PointOfSale->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *PointOfSaleService) Delete(ctx context.Context, req *user_service.PointOfSalePrimaryKey) (resp *user_service.Empty, err error) {

	i.log.Info("---DeletePointOfSale------>", logger.Any("req", req))

	err = i.strg.PointOfSale().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeletePointOfSale->PointOfSale->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &user_service.Empty{}, nil
}
