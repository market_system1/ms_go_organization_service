-- CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

-- Филиал
CREATE TABLE IF NOT EXISTS "branch" (
    "id" UUID PRIMARY KEY,
    "branch_id" VARCHAR(20) UNIQUE NOT NULL,
    "name" VARCHAR(50) NOT NULL,
    "address" VARCHAR(50) NOT NULL,
    "phone_number" VARCHAR(20) NOT NULL,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);


-- Точки Продаж
CREATE TABLE IF NOT EXISTS "points_of_sale" (
    "id" UUID PRIMARY KEY,
    "name" VARCHAR(50) UNIQUE NOT NULL,
    "branch_id" UUID  NOT NULL REFERENCES "branch"("id") ,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);



 CREATE TYPE USER_TYPE AS ENUM('CASHIER', 'ADMIN', 'WARE_HOUSE');
-- Сотрудники
CREATE TABLE IF NOT EXISTS "users" (
    "id" UUID PRIMARY KEY,
    "first_name" VARCHAR(50),
    "last_name" VARCHAR(50),
    "phone_number" VARCHAR(60),
    "login" VARCHAR(10) UNIQUE,
    "password" VARCHAR,
    "points_of_sale_id" UUID NOT NULL REFERENCES "points_of_sale" ("id"),
    "user_type" USER_TYPE,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);

-- Поставщик
CREATE TABLE IF NOT EXISTS "provider" (
    "id" UUID PRIMARY KEY,
    "name" VARCHAR(50) UNIQUE NOT NULL,
    "phone_number" VARCHAR(20) UNIQUE NOT NULL,
    "active" BOOLEAN DEFAULT FALSE,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP
);