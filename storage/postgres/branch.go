package postgres

import (
	"context"
	"database/sql"
	"errors"
	"market_system/ms_go_user_service/genproto/user_service"
	"market_system/ms_go_user_service/pkg/helper"
	"market_system/ms_go_user_service/storage"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type BranchRepo struct {
	db *pgxpool.Pool
}

func NewBranchRepo(db *pgxpool.Pool) storage.BranchRepoI {
	return &BranchRepo{
		db: db,
	}
}

func (c *BranchRepo) Create(ctx context.Context, req *user_service.CreateBranch) (resp *user_service.BranchPrimaryKey, err error) {

	var (
		id = uuid.New()
	)

	if len(req.GetName()) == 0 {
		return nil, errors.New("name required")
	}

	branchIncrementId, err := helper.NewIncrementId(c.db, "branch_id", "branch", req.GetName(), 8)

	if err != nil {
		return nil, err
	}

	query := `INSERT INTO "branch" (
				id,
				branch_id,
				name,
				address,
				phone_number,
				updated_at
			) VALUES ($1, $2, $3, $4, $5, now())
		`

	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		branchIncrementId(),
		req.GetName(),
		req.GetAddress(),
		req.GetPhoneNumber(),
	)

	if err != nil {
		return nil, err
	}

	return &user_service.BranchPrimaryKey{Id: id.String()}, nil
}

func (c *BranchRepo) GetByPKey(ctx context.Context, req *user_service.BranchPrimaryKey) (resp *user_service.Branch, err error) {

	query := `
		SELECT
			id,
			branch_id,
			name,
			address,
			phone_number,
			created_at,
			updated_at
		FROM "branch"
		WHERE id = $1
	`

	var (
		id           sql.NullString
		branch_id    sql.NullString
		name         sql.NullString
		address      sql.NullString
		phone_number sql.NullString
		createdAt    sql.NullString
		updatedAt    sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&branch_id,
		&name,
		&address,
		&phone_number,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &user_service.Branch{
		Id:          id.String,
		BranchId:    branch_id.String,
		Name:        name.String,
		Address:     address.String,
		PhoneNumber: phone_number.String,
		CreatedAt:   createdAt.String,
		UpdatedAt:   updatedAt.String,
	}

	return
}

func (c *BranchRepo) GetAll(ctx context.Context, req *user_service.GetListBranchRequest) (resp *user_service.GetListBranchResponse, err error) {

	resp = &user_service.GetListBranchResponse{}

	var (
		query  string
		limit  = " LIMIT 10"
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			branch_id,
			name,
			address,
			phone_number,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "branch"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.GetLimit()
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		if req.GetLimit() < 0 {
			params["offset"] = req.GetOffset() * 10
		} else {
			params["offset"] = req.GetOffset() * req.GetLimit()
		}
	}

	if len(req.GetName()) > 0 {
		filter += "  AND name ILIKE '%" + req.GetName() + "%'"
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id           sql.NullString
			branch_id    sql.NullString
			name         sql.NullString
			address      sql.NullString
			phone_number sql.NullString
			createdAt    sql.NullString
			updatedAt    sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&branch_id,
			&name,
			&address,
			&phone_number,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Branches = append(resp.Branches, &user_service.Branch{
			Id:          id.String,
			BranchId:    branch_id.String,
			Name:        name.String,
			Address:     address.String,
			PhoneNumber: phone_number.String,
			CreatedAt:   createdAt.String,
			UpdatedAt:   updatedAt.String,
		})
	}

	return
}

func (c *BranchRepo) Update(ctx context.Context, req *user_service.UpdateBranch) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE "branch"	SET
			branch_id = :branch_id,
			name = :name,
			address = :address,
			phone_number = :phone_number,
			updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":           req.GetId(),
		"branch_id":    req.GetBranchId(),
		"name":         req.GetName(),
		"address":      req.GetAddress(),
		"phone_number": req.GetPhoneNumber(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *BranchRepo) Delete(ctx context.Context, req *user_service.BranchPrimaryKey) error {

	query := `DELETE FROM "branch" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
