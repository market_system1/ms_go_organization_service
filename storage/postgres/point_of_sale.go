package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"market_system/ms_go_user_service/genproto/user_service"
	"market_system/ms_go_user_service/pkg/helper"
	"market_system/ms_go_user_service/storage"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type PointOfSaleRepo struct {
	db *pgxpool.Pool
}

func NewPointOfSaleRepo(db *pgxpool.Pool) storage.PointOfSaleRepoI {
	return &PointOfSaleRepo{
		db: db,
	}
}

func (c *PointOfSaleRepo) Create(ctx context.Context, req *user_service.CreatePointOfSale) (resp *user_service.PointOfSalePrimaryKey, err error) {
	fmt.Println(req)
	var id = uuid.New()

	query := `INSERT INTO "points_of_sale" (
				id,
				name,
				branch_id,
				updated_at
			) VALUES ($1, $2, $3, now())
			`
	fmt.Println("salom bu Create")

	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.GetName(),
		req.GetBranchId(),
	)

	if err != nil {
		return nil, err
	}

	return &user_service.PointOfSalePrimaryKey{Id: id.String()}, nil
}

func (c *PointOfSaleRepo) GetByPKey(ctx context.Context, req *user_service.PointOfSalePrimaryKey) (resp *user_service.PointOfSale, err error) {

	query := `
		SELECT
			id,
			name,
			branch_id,	
			created_at,
			updated_at
		FROM "points_of_sale"
		WHERE id = $1
	`

	var (
		id        sql.NullString
		name      sql.NullString
		branch_id sql.NullString
		createdAt sql.NullString
		updatedAt sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&name,
		&branch_id,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &user_service.PointOfSale{
		Id:        id.String,
		Name:      name.String,
		BranchId:  branch_id.String,
		CreatedAt: createdAt.String,
		UpdatedAt: updatedAt.String,
	}

	return
}

func (c *PointOfSaleRepo) GetAll(ctx context.Context, req *user_service.GetListPointOfSaleRequest) (resp *user_service.GetListPointOfSaleResponse, err error) {

	resp = &user_service.GetListPointOfSaleResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			name,
			branch_id,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "points_of_sale"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	if len(req.GetName()) > 0 {
		filter += "  AND name ILIKE '%" + req.GetName() + "%'"
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id        sql.NullString
			name      sql.NullString
			branch_id sql.NullString
			createdAt sql.NullString
			updatedAt sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&name,
			&branch_id,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.PointOfSales = append(resp.PointOfSales, &user_service.PointOfSale{
			Id:        id.String,
			Name:      name.String,
			BranchId:  branch_id.String,
			CreatedAt: createdAt.String,
			UpdatedAt: updatedAt.String,
		})
	}

	return
}

func (c *PointOfSaleRepo) Update(ctx context.Context, req *user_service.UpdatePointOfSale) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE "points_of_sale"	SET
			name = :name,
			branch_id = :branch_id,
			updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":        req.GetId(),
		"name":      req.GetName(),
		"branch_id": req.GetBranchId(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *PointOfSaleRepo) Delete(ctx context.Context, req *user_service.PointOfSalePrimaryKey) error {

	query := `DELETE FROM "points_of_sale" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
