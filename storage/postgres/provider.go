package postgres

import (
	"context"
	"database/sql"
	"market_system/ms_go_user_service/genproto/user_service"
	"market_system/ms_go_user_service/pkg/helper"
	"market_system/ms_go_user_service/storage"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type ProviderRepo struct {
	db *pgxpool.Pool
}

func NewProviderRepo(db *pgxpool.Pool) storage.ProviderRepoI {
	return &ProviderRepo{
		db: db,
	}
}

func (c *ProviderRepo) Create(ctx context.Context, req *user_service.CreateProvider) (resp *user_service.ProviderPrimaryKey, err error) {

	var id = uuid.New()

	query := `INSERT INTO "provider" (
				id,
				name,
				phone_number,
				active,
				updated_at
			) VALUES ($1, $2, $3, $4, now())
		`

	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.GetName(),
		req.GetPhoneNumber(),
		req.GetActive(),
	)

	if err != nil {
		return nil, err
	}

	return &user_service.ProviderPrimaryKey{Id: id.String()}, nil
}

func (c *ProviderRepo) GetByPKey(ctx context.Context, req *user_service.ProviderPrimaryKey) (resp *user_service.Provider, err error) {

	query := `
		SELECT
			id,
			name,
			phone_number,
			active,
			created_at,
			updated_at
		FROM "provider"
		WHERE id = $1
	`

	var (
		id           sql.NullString
		name         sql.NullString
		phone_number sql.NullString
		active       sql.NullBool
		createdAt    sql.NullString
		updatedAt    sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&name,
		&phone_number,
		&active,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &user_service.Provider{
		Id:          id.String,
		Name:        name.String,
		PhoneNumber: phone_number.String,
		Active:      active.Bool,
		CreatedAt:   createdAt.String,
		UpdatedAt:   updatedAt.String,
	}

	return
}

func (c *ProviderRepo) GetAll(ctx context.Context, req *user_service.GetListProviderRequest) (resp *user_service.GetListProviderResponse, err error) {

	resp = &user_service.GetListProviderResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			name,
			phone_number,
			active,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "provider"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	if len(req.GetName()) > 0 {
		filter += "  AND name ILIKE '%" + req.GetName() + "%'"
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id           sql.NullString
			name         sql.NullString
			phone_number sql.NullString
			active       sql.NullBool
			createdAt    sql.NullString
			updatedAt    sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&name,
			&phone_number,
			&active,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Providers = append(resp.Providers, &user_service.Provider{
			Id:          id.String,
			Name:        name.String,
			PhoneNumber: phone_number.String,
			Active:      active.Bool,
			CreatedAt:   createdAt.String,
			UpdatedAt:   updatedAt.String,
		})
	}

	return
}

func (c *ProviderRepo) Update(ctx context.Context, req *user_service.UpdateProvider) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE "provider"	SET
			name = :name,
			phone_number = :phone_number,
			active = :active,
			updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":           req.GetId(),
		"name":         req.GetName(),
		"phone_number": req.GetPhoneNumber(),
		"active":       req.GetActive(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *ProviderRepo) Delete(ctx context.Context, req *user_service.ProviderPrimaryKey) error {

	query := `DELETE FROM "provider" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
