package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"

	"market_system/ms_go_user_service/genproto/user_service"
	"market_system/ms_go_user_service/pkg/helper"
	"market_system/ms_go_user_service/storage"
)

type UserRepo struct {
	db *pgxpool.Pool
}

func NewUserRepo(db *pgxpool.Pool) storage.UserRepoI {
	return &UserRepo{
		db: db,
	}
}

func (c *UserRepo) Create(ctx context.Context, req *user_service.CreateUser) (resp *user_service.UserPrimaryKey, err error) {

	var id = uuid.New()

	query := `INSERT INTO "users" (
				id,
				first_name,
				last_name,
				phone_number,
				login,
				password,
				points_of_sale_id,
				user_type,
				updated_at
			) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, now())
		`

	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.GetFirstName(),
		req.GetLastName(),
		req.GetPhoneNumber(),
		req.GetLogin(),
		req.GetPassword(),
		req.GetPointsOfSaleId(),
		req.GetUserType(),
	)

	if err != nil {
		return nil, err
	}

	return &user_service.UserPrimaryKey{Id: id.String()}, nil
}

func (c *UserRepo) GetByPKey(ctx context.Context, req *user_service.UserPrimaryKey) (resp *user_service.User, err error) {

	query := `
		SELECT
			id,
			first_name,
			last_name,
			phone_number,
			login,
			password,
			points_of_sale_id,
			user_type,
			created_at,
			updated_at
		FROM "users"
		WHERE id = $1
	`

	var (
		id                sql.NullString
		first_name        sql.NullString
		last_name         sql.NullString
		phone_number      sql.NullString
		login             sql.NullString
		password          sql.NullString
		points_of_sale_id sql.NullString
		user_type         sql.NullString
		createdAt         sql.NullString
		updatedAt         sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&first_name,
		&last_name,
		&phone_number,
		&login,
		&password,
		&points_of_sale_id,
		&user_type,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &user_service.User{
		Id:             id.String,
		FirstName:      first_name.String,
		LastName:       last_name.String,
		PhoneNumber:    phone_number.String,
		Login:          login.String,
		Password:       password.String,
		PointsOfSaleId: points_of_sale_id.String,
		UserType:       user_type.String,
		CreatedAt:      createdAt.String,
		UpdatedAt:      updatedAt.String,
	}

	return
}

func (c *UserRepo) GetAll(ctx context.Context, req *user_service.GetListUserRequest) (resp *user_service.GetListUserResponse, err error) {

	resp = &user_service.GetListUserResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			first_name,
			last_name,
			phone_number,
			login,
			password,
			points_of_sale_id,
			user_type,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "users"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.GetLimit()
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.GetOffset()
	}
	if len(req.GetFirstName()) > 0 {
		filter += "  AND first_name ILIKE '%" + req.GetFirstName() + "%'"
		// params["first_name"] = req.GetFirstName()
	}
	if len(req.GetLastName()) > 0 {
		filter += " AND last_name ILIKE '%" + req.GetLastName() + "%'"
		// params["last_name"] = req.GetLastName()
	}
	if len(req.GetPhoneNumber()) > 0 {
		filter += " AND phone_number ILIKE '%" + req.GetPhoneNumber() + "%'"
		// params["phone_number"] = req.GetPhoneNumber()
	}

	query += filter + sort + offset + limit
	fmt.Println(params)
	query, args := helper.ReplaceQueryParams(query, params)
	fmt.Println(">>>>>>USER>>>>>>>", query)
	rows, err := c.db.Query(ctx, query, args...)
	// fmt.Println(">>>>>>USER>>>>>>>", query)
	defer rows.Close()

	if err != nil {
		return resp, err
	}
	fmt.Println(">>>>>>USER>>>>>>>", args)

	for rows.Next() {
		var (
			id                sql.NullString
			first_name        sql.NullString
			last_name         sql.NullString
			phone_number      sql.NullString
			login             sql.NullString
			password          sql.NullString
			points_of_sale_id sql.NullString
			user_type         sql.NullString
			createdAt         sql.NullString
			updatedAt         sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&first_name,
			&last_name,
			&phone_number,
			&login,
			&password,
			&points_of_sale_id,
			&user_type,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Users = append(resp.Users, &user_service.User{
			Id:             id.String,
			FirstName:      first_name.String,
			LastName:       last_name.String,
			PhoneNumber:    phone_number.String,
			Login:          login.String,
			Password:       password.String,
			PointsOfSaleId: points_of_sale_id.String,
			UserType:       user_type.String,
			CreatedAt:      createdAt.String,
			UpdatedAt:      updatedAt.String,
		})
	}

	return
}

func (c *UserRepo) Update(ctx context.Context, req *user_service.UpdateUser) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "users"
			SET
				first_name = :first_name,
				last_name = :last_name,
				phone_number = :phone_number,
				login = :login,
				password = :password,
				points_of_sale_id = :points_of_sale_id,
				user_type = :user_type,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":                req.GetId(),
		"first_name":        req.GetFirstName(),
		"last_name":         req.GetLastName(),
		"phone_number":      req.GetPhoneNumber(),
		"login":             req.GetLogin(),
		"password":          req.GetPassword(),
		"points_of_sale_id": req.GetPointsOfSaleId(),
		"user_type":         req.GetUserType(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *UserRepo) Delete(ctx context.Context, req *user_service.UserPrimaryKey) error {

	query := `DELETE FROM "users" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
