package storage

import (
	"context"
	"market_system/ms_go_user_service/genproto/user_service"
)

type StorageI interface {
	CloseDB()
	User() UserRepoI
	Branch() BranchRepoI
	PointOfSale() PointOfSaleRepoI
	Provider() ProviderRepoI
}

type UserRepoI interface {
	Create(ctx context.Context, req *user_service.CreateUser) (resp *user_service.UserPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *user_service.UserPrimaryKey) (resp *user_service.User, err error)
	GetAll(ctx context.Context, req *user_service.GetListUserRequest) (resp *user_service.GetListUserResponse, err error)
	Update(ctx context.Context, req *user_service.UpdateUser) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *user_service.UserPrimaryKey) error
}

type BranchRepoI interface {
	Create(ctx context.Context, req *user_service.CreateBranch) (resp *user_service.BranchPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *user_service.BranchPrimaryKey) (resp *user_service.Branch, err error)
	GetAll(ctx context.Context, req *user_service.GetListBranchRequest) (resp *user_service.GetListBranchResponse, err error)
	Update(ctx context.Context, req *user_service.UpdateBranch) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *user_service.BranchPrimaryKey) error
}

type PointOfSaleRepoI interface {
	Create(ctx context.Context, req *user_service.CreatePointOfSale) (resp *user_service.PointOfSalePrimaryKey, err error)
	GetByPKey(ctx context.Context, req *user_service.PointOfSalePrimaryKey) (resp *user_service.PointOfSale, err error)
	GetAll(ctx context.Context, req *user_service.GetListPointOfSaleRequest) (resp *user_service.GetListPointOfSaleResponse, err error)
	Update(ctx context.Context, req *user_service.UpdatePointOfSale) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *user_service.PointOfSalePrimaryKey) error
}
type ProviderRepoI interface {
	Create(ctx context.Context, req *user_service.CreateProvider) (resp *user_service.ProviderPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *user_service.ProviderPrimaryKey) (resp *user_service.Provider, err error)
	GetAll(ctx context.Context, req *user_service.GetListProviderRequest) (resp *user_service.GetListProviderResponse, err error)
	Update(ctx context.Context, req *user_service.UpdateProvider) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *user_service.ProviderPrimaryKey) error
}
